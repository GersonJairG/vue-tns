export default [
  {
    label: 'EFECTIVO',
    value: 'EF',
  },
  {
    label: 'CHEQUE',
    value: 'CH',
  },
  {
    label: 'TARJETA DE CREDITO',
    value: 'TC',
  },
  {
    label: 'CONSIGNACION',
    value: 'CO',
  },
  {
    label: 'TARJETA DÉBITO',
    value: 'TD',
  },
  {
    label: 'TRANSFERENCIA ELECTRONICA',
    value: 'TE',
  },
  {
    label: 'GLOSAS',
    value: 'GL',
  },
  {
    label: 'AJUSTE',
    value: 'AJ',
  },
  {
    label: 'CUENTA POR PAGAR',
    value: 'I1',
  },
];

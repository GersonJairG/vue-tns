export default [
  {
    label: 'ADVIL MAX',
    value: '001',
  },
  {
    label: 'ACETAMINOFEN',
    value: '002',
  },
  {
    label: 'ALCOHOL ANTISEPTICO 700ML',
    value: '003',
  },
  {
    label: 'MINOXIDIL SOLUCION TOPICA  5% X 60ML',
    value: '004',
  },
  {
    label: 'DICLOFENACO 50MG X 30 TABLETAS',
    value: '005',
  },
  {
    label: 'LOSARTAN 100MG X 30 TABLETAS',
    value: '006',
  },
  {
    label: 'ALKA SELTZER',
    value: '007',
  },
  {
    label: 'BUSCAPINA 10MG CAJA X 20 GRAGEAS',
    value: '008',
  },
  {
    label: 'DOLEX NIÑOS JARABE FRUTAS FRASCO x 90ML',
    value: '009',
  },
  {
    label: 'ENTEROGERMINA',
    value: '010',
  },
  {
    label: 'PEDIALYTE+ZINC 45 FRESA FRASCO X 500ML',
    value: '011',
  },
  {
    label: 'VICK VAPORUB UNGUENTO FRASCO X 50 GRAMOS',
    value: '012',
  },
  {
    label: 'FINIGAX 125MG',
    value: '013',
  },
  {
    label: 'NORAVER GRIPA NOCHE SABOR PANELA X 6 SOBRES',
    value: '014',
  },
  {
    label: 'BETAMETASONA CREMA 0.1% TUBO x 20GR',
    value: '015',
  },
  {
    label: 'BIOGAIA PROTECTIS',
    value: '016',
  },
  {
    label: 'CIPROFLOXACINA MK 500MG',
    value: '017',
  },
  {
    label: 'LASIX 40MG',
    value: '018',
  },
  {
    label: 'LEVEMIR FLEXPEN 100U/ML CAJA CON 5 DISPOSITIVOS X 3ML',
    value: '019',
  },
  {
    label: 'VIAGRA 50MG',
    value: '020',
  },
  {
    label: 'COLAGENO FRASCO X 60 CAPSULAS',
    value: '021',
  },
  {
    label: 'FOLIC ACID 400MCG GOOD NATURAL X 250 TABLETAS',
    value: '022',
  },
  {
    label: 'GINKGO BILOBA 500MG FRASCO x 90 CAPSULAS',
    value: '023',
  },
  {
    label: 'REDOXITOS TOTAL GOMAS X 25 UND',
    value: '024',
  },
  {
    label: 'SUPLEMENTO DIETARIO CENTRUM MEN X 60 TABLETAS',
    value: '025',
  },
  {
    label: 'SUPLEMENTO DIETARIO CENTRUM WOMEN X 60 TABLETAS',
    value: '026',
  },
  {
    label: 'VITAMINA E 1000 IU',
    value: '027',
  },
  {
    label: 'VITAMINA E 1000IU GOOD NATURAL X 50 CAPSULAS',
    value: '028',
  },
];

export default [
  {
    label: 'Bancolombia',
    value: '01',
  },
  {
    label: 'Banco Bogotá',
    value: '02',
  },
  {
    label: 'Colpatria',
    value: '03',
  },
  {
    label: 'BBVA',
    value: '04',
  },
  {
    label: 'Banco Caja Social',
    value: '05',
  },
  {
    label: 'Davivienda',
    value: '06',
  },
  {
    label: 'Banco Popular',
    value: '07',
  },
];

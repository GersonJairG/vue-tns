export const getFormat = (date) => {
  const day = `0${date.getDate()}`.slice(-2);
  const month = `0${date.getMonth() + 1}`.slice(-2);
  const year = date.getYear() + 1900;
  return `${day}/${month}/${year}`;
};

export default getFormat;

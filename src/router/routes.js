
const routes = [
  {
    path: '/options',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
    ],
  },
  {
    path: '/', component: () => import('pages/Home.vue'),
  },
  {
    path: '/services', component: () => import('pages/Services.vue'),
  },
  {
    path: '/documents', component: () => import('pages/Documents.vue'),
  },
  {
    path: '/order-sell', component: () => import('pages/OrderSell.vue'),
  },
  {
    path: '/receipt-master', component: () => import('pages/ReceiptMaster.vue'),
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
